(function () {
    var topScoreSave = JSON.parse(localStorage.getItem('topScoreSave')) || [];
    var scorePoint = 0;
    var healthPoint = JSON.parse(localStorage.getItem('healthPoint')) || 3;
    drawTable();

    var level = [
        '**************',
        '**************',
        '**************',
        '**************'
    ];

    var gameLoop;
    var gameSpeed = 20;
    var ballMovementSpeed = 3;


    var bricks = [];
    var bricksMargin = 1;
    var bricksWidth = 0;
    var bricksHeight = 18;
    var numberColor = [{ nm: 0, cl: 'blue' }, { nm: 1, cl: 'red' }, { nm: 2, cl: 'green' }, { nm: 3, cl: 'yellow' }, { nm: 4, cl: 'cyan' }];



    var ball = JSON.parse(localStorage.getItem('ball')) || {
        width: 6,
        height: 6,
        left: 0,
        top: 0,
        speedLeft: 0,
        speedTop: 0
    };

    var healthBubble = {
        width: 6,
        height: 6,
        left: 0,
        top: 0,
        speedLeft: 0,
        speedTop: 0
    };

    var paddle = JSON.parse(localStorage.getItem('paddle')) || {
        width: 100,
        height: 6,
        left: (document.getElementById('breakout').offsetWidth / 2) - 30,
        top: document.getElementById('breakout').offsetHeight - 40
    };


    function startGame() {
        resetBall();
        buildLevel();
        createBricks();
        updateObjects();
    }

    function createStartButton() {

        var startButton = document.createElement('BUTTON')
        startButton.id = 'start';
        startButton.style.position = 'absolute';
        startButton.innerText = 'Start';
        startButton.style.width = '80px';
        startButton.style.height = '40px';
        startButton.style.left = '45%';
        startButton.style.top = '45%';
        startButton.onclick = startGameLoop;
        return startButton;
    }

    function drawTable() {
        document.body.style.background = '#0E5CAD';
        document.body.style.font = '18px Orbitron';
        document.body.style.color = '#FFF';

        var breakout = document.createElement('div');
        var paddle = document.createElement('div');
        var ball = document.createElement('div');


        breakout.id = 'breakout';
        breakout.style.width = '800px';
        breakout.style.height = '600px';
        breakout.style.position = 'fixed';
        breakout.style.left = '50%';
        breakout.style.top = '50%';
        breakout.style.transform = 'translate(-50%, -50%)';
        breakout.style.background = '#000000';

        paddle.id = 'paddle';
        paddle.style.background = '#E80505';
        paddle.style.position = 'absolute';
        paddle.style.boxShadow = '0 15px 6px -2px rgba(0,0,0,.6)';

        ball.className = 'ball';
        ball.style.position = 'absolute';
        ball.style.background = '#FFF';
        ball.style.boxShadow = '0 15px 6px -1px rgba(0,0,0,.6)';
        ball.style.borderRadius = '50%';
        ball.style.zIndex = '9';

        breakout.appendChild(paddle);
        breakout.appendChild(ball);

        var health = document.createElement('div')
        health.id = 'health';
        health.innerText = 'Health = ' + healthPoint;
        health.dataset.health = healthPoint;
        health.style.position = 'relative';
        health.style.top = '-20px'
        health.style.float = 'left';

        var score = document.createElement('div')
        score.id = 'score';
        score.innerText = 'Score = ' + scorePoint;
        score.dataset.score = '0';
        score.style.position = 'relative';
        score.style.top = '-20px'
        score.style.float = 'left';

        var topScore = document.createElement('div')
        topScore.id = 'topScore';
        topScore.innerText = 'Top Score = '
        topScoreSave.forEach(function (element) {
            topScore.innerText += ' | ' + element;
        });
        topScore.style.position = 'relative';
        topScore.style.top = '-20px'
        topScore.style.float = 'left';


        breakout.append(health);
        breakout.append(score);
        breakout.append(topScore);

        var playerbutton = document.createElement('BUTTON')
        playerbutton.id = 'player';
        playerbutton.style.position = 'relative';
        playerbutton.innerText = 'Player';
        playerbutton.style.width = '80px';
        playerbutton.style.height = '40px';
        playerbutton.style.top = '100%';
        playerbutton.style.float = 'right';
        playerbutton.onclick = startGameLoop;

        var cpubutton = document.createElement('BUTTON')
        cpubutton.id = 'cpu';
        cpubutton.style.position = 'relative';
        cpubutton.innerText = 'Cpu';
        cpubutton.style.width = '80px';
        cpubutton.style.height = '40px';
        cpubutton.style.top = '100%';
        cpubutton.style.float = 'right';
        cpubutton.onclick = cpuPlay;

        breakout.append(cpubutton);
        breakout.append(playerbutton);
        breakout.append(createStartButton());

        document.body.appendChild(breakout);
    }

    function removeElement(element) {
        if (element && element.parentNode) {
            element.parentNode.removeChild(element);
        }
    }

    function buildLevel() {
        var arena = document.getElementById('breakout');

        bricks = JSON.parse(localStorage.getItem('bricks')) || [];

        if (!bricks.length) {
            for (var row = 0; row < level.length; row++) {
                for (var column = 0; column <= level[row].length; column++) {

                    if (!level[row][column] || level[row][column] === ' ') {
                        continue;
                    }

                    bricksWidth = (arena.offsetWidth - bricksMargin * 2) / level[row].length;
                    nmcl = numberColor[Math.floor(Math.random() * numberColor.length)];
                    bricks.push({
                        left: bricksMargin * 2 + (bricksWidth * column),
                        top: bricksHeight * row + 60,
                        width: bricksWidth - bricksMargin * 2,
                        height: bricksHeight - bricksMargin * 2,
                        nm: nmcl.nm,
                        cl: nmcl.cl
                    });
                }
            }
        }
    }

    function removeBricks() {
        document.querySelectorAll('.brick').forEach(function (brick) {
            removeElement(brick);
        });
    }

    function createBricks() {
        removeBricks();

        var arena = document.getElementById('breakout');

        bricks.forEach(function (brick, index) {
            if (brick.removed) {
                scorePoint++;
                return;
            }
            var element = document.createElement('div');

            element.id = 'brick-' + index;
            element.className = 'brick';
            element.style.left = brick.left + 'px';
            element.style.top = brick.top + 'px';
            element.style.width = brick.width + 'px';
            element.style.height = brick.height + 'px';
            element.style.background = brick.cl
            element.style.position = 'absolute';
            element.style.boxShadow = '0 15px 20px 0px rgba(0,0,0,.4)';
            element.style.textAlign = 'center';
            element.innerText = brick.nm;

            arena.appendChild(element)
        });
        document.getElementById('score').innerText = 'Score = ' + scorePoint;
    }

    function updateObjects() {
        document.getElementById('paddle').style.width = paddle.width + 'px';
        document.getElementById('paddle').style.height = paddle.height + 'px';
        document.getElementById('paddle').style.left = paddle.left + 'px';
        document.getElementById('paddle').style.top = paddle.top + 'px';

        document.querySelector('.ball').style.width = ball.width + 'px';
        document.querySelector('.ball').style.height = ball.height + 'px';
        document.querySelector('.ball').style.left = ball.left + 'px';
        document.querySelector('.ball').style.top = ball.top + 'px';

        moveBubble();
    }

    function resetBall() {
        var arena = document.getElementById('breakout');

        ball.left = (arena.offsetWidth / 2) - (ball.width / 2);
        ball.top = (arena.offsetHeight / 1.6) - (ball.height / 2);

        ball.speedLeft = 1;
        ball.speedTop = ballMovementSpeed;

        if (Math.round(Math.random() * 1)) {
            ball.speedLeft = -ballMovementSpeed;
        }

        document.querySelector('.ball').style.left = ball.left + 'px';
        document.querySelector('.ball').style.top = ball.top + 'px';
    }

    function movePaddle(event) {
        var arena = document.getElementById('breakout');
        var arenaRect = arena.getBoundingClientRect();
        var arenaWidth = arena.offsetWidth;
        var mouseX = event.clientX - arenaRect.x;
        var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;

        if (mouseX <= halfOfPaddle) {
            mouseX = halfOfPaddle;
        }

        if (mouseX >= arenaWidth - halfOfPaddle) {
            mouseX = arenaWidth - halfOfPaddle;
        }

        paddle.left = mouseX - halfOfPaddle;
    }

    function movePaddleKeyboardOrPause(event) {
        var arena = document.getElementById('breakout');
        var arenaRect = arena.getBoundingClientRect();
        var arenaWidth = arena.offsetWidth;
        var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;
        if (event.keyCode == '37') {
            if (paddle.left >= 15) {
                paddle.left -= 15;
            }
        }
        else if (event.keyCode == '39') {
            if (paddle.left < arenaWidth - (halfOfPaddle * 2))
                paddle.left += 15;
        }
        else {
            pauseGame();
        }
    }

    function movePaddleCpu() {
        var arena = document.getElementById('breakout');
        var arenaRect = arena.getBoundingClientRect();
        var arenaWidth = arena.offsetWidth;
        var halfOfPaddle = document.getElementById('paddle').offsetWidth / 2;
        paddle.left = ball.left - halfOfPaddle;


    }

    function moveBall() {

        detectCollision();

        var arena = document.getElementById('breakout');

        ball.top += ball.speedTop;
        ball.left += ball.speedLeft;

        if (ball.left <= 0 || ball.left + ball.width >= arena.offsetWidth) {
            ball.speedLeft = -ball.speedLeft;
        }

        if (ball.top <= 0 || ball.top + ball.height >= arena.offsetHeight) {
            ball.speedTop = -ball.speedTop;
        }

        if (ball.top + ball.height >= arena.offsetHeight) {
            resetBall();
            var health = document.getElementById('health').dataset.health
            if (health == '0') {
                topScoreSave.push(document.getElementById('score').dataset.score);
                topScoreSave.sort(function (a, b) { return b - a });
                topScoreSave.splice(3, topScoreSave.length - 3);
                localStorage.setItem('topScoreSave', JSON.stringify(topScoreSave));
                document.getElementById('breakout').remove();
                localStorage.setItem('healthPoint', JSON.stringify(healthPoint + 3));
                healthPoint += 3;
                clearInterval(gameLoop);
                drawTable();
                startGame()
            }
            else {
                health--;
                healthPoint = health;
                document.getElementById('health').innerText = 'Health = ' + health;
                document.getElementById('health').dataset.health = health
            }
        }
    }

    function detectCollision() {
        if (ball.top + ball.height >= paddle.top
            && ball.top + ball.height <= paddle.top + paddle.height
            && ball.left >= paddle.left
            && ball.left <= paddle.left + paddle.width
        ) {

            px = ball.speedLeft * Math.cos(20) - ball.speedTop * Math.sin(20);
            py = ball.speedLeft * Math.sin(20) + ball.speedTop * Math.cos(20);

            ball.speedTop = px
            if (py < 0) py *= -1;
            ball.speedLeft = py
        }

        for (var i = 0; i < bricks.length; i++) {
            var brick = bricks[i];

            if (ball.top + ball.height >= brick.top
                && ball.top <= brick.top + brick.height
                && ball.left + ball.width >= brick.left
                && ball.left <= brick.left + brick.width
                && !brick.removed
            ) {
                ball.speedTop = -ball.speedTop;
                breakBrickAtIndex(i);
                var random = Math.floor(Math.random() * 101);
                if (random <= 10) {
                    createBubble(brick.left, brick.top);
                }
                break;
            }
        }


        var bubbles = document.getElementsByClassName('hpdrop');
        Array.prototype.forEach.call(bubbles, function (el) {
            var top = parseInt(el.style.top)
            var height = parseInt(el.style.height)
            var left = parseInt(el.style.left)

            if (top + height >= paddle.top
                && top + height <= paddle.top + paddle.height
                && left >= paddle.left
                && left <= paddle.left + paddle.width
            ) { el.remove();
                var health = document.getElementById('health').dataset.health++;
                document.getElementById('health').innerText = 'Health = ' + health;
            }
        });
    }


    function createBubble(x, y) {
        var arena = document.getElementById('breakout');
        var hpdrop = document.createElement('div');
        hpdrop.className = 'hpdrop';
        hpdrop.style.position = 'absolute';
        hpdrop.style.background = 'Red';
        hpdrop.style.top = y;
        hpdrop.style.left = x;
        hpdrop.style.width = '15px';
        hpdrop.style.height = '15px';
        hpdrop.style.borderRadius = '50%'
        arena.appendChild(hpdrop);

    }
    function moveBubble() {
        var arena = document.getElementById('breakout');
        var arenaHeight = arena.offsetHeight;

        var bubbles = document.getElementsByClassName('hpdrop');
        Array.prototype.forEach.call(bubbles, function (el) {
            var top = parseInt(el.style.top);
            top++;
            el.style.top = top + 'px';

            if (top >= arenaHeight) {
                el.remove();
            }
        });
    }

    function breakBrickAtIndex(index) {
        var str = 'brick-' + index
        if (!bricks[index].removed) {
            var element = document.getElementById(str);
            if (bricks[index].nm == 0) {
                element.remove();
                bricks[index].removed = true;
                ball.speedTop += .1;
            } else {
                bricks[index].nm--;
                element.innerText = bricks[index].nm;
                element.style.background = numberColor[bricks[index].nm].cl;
            }

            document.getElementById('score').dataset.score++;
            document.getElementById('score').innerText = 'Score = ' + document.getElementById('score').dataset.score;
        }

    }

    function setEvents() {
        document.addEventListener('mousemove', movePaddle);
        document.addEventListener('keydown', movePaddleKeyboardOrPause);
    }



    function cpuPlay() {
        document.removeEventListener('mousemove', movePaddle);

        clearInterval(gameLoop);
        gameLoop = setInterval(function () {
            moveBall();
            movePaddleCpu();
            updateObjects();
        }, gameSpeed);
        if (document.getElementById('start')) { document.getElementById('start').remove(); }

    }

    function startGameLoop() {
        clearInterval(gameLoop);
        if (document.getElementById('start')) { document.getElementById('start').remove(); }
        document.addEventListener('mousemove', movePaddle);

        gameLoop = setInterval(function () {
            moveBall();
            updateObjects();
        }, gameSpeed);
    }

    function pauseGame() {
        clearInterval(gameLoop);
        document.getElementById('breakout').append(createStartButton());


        if (localStorage.getItem(bricks)) {
            localStorage.removeItem('bricks');
            localStorage.setItem('bricks', JSON.stringify(bricks));
        } else {
            localStorage.setItem('bricks', JSON.stringify(bricks));
        }

        localStorage.setItem('ball', JSON.stringify(ball));
        localStorage.setItem('healthPoint', JSON.stringify(healthPoint));
        localStorage.setItem('paddle', JSON.stringify(paddle));
        topScoreSave.sort(function (a, b) { return a - b });
        localStorage.setItem('topScoreSave', JSON.stringify(topScoreSave));

    }

    setEvents();
    startGame();

})();